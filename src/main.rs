use std::env;
use std::process;
use std::borrow::BorrowMut;

fn is_valid(player1: &[usize], player2: &[usize]) -> bool {
    // verify each individual player's pieces are unique or in the starting/ending pools
    let mut seen = [false; 16]; // non-starting or ending pools

    // The placement logic ensures that these constraints are respected, so this code could
    // be skipped when called from search_games_dfs. Skipping this check halves the run-time.
    for player in [player1, player2].iter() {
        for placement in player.iter() {
            match *placement {
                0 => (),
                15 => (),
                place if seen[place] =>
                    return false, // duplicate
                place =>
                    seen[place] = true
            }
        }
        seen.fill(false);  // reset for next player
    }

    for player in [player1, player2].iter() {
        for placement in player.iter() {
            match *placement {
                place if place >= 5 && place <= 12 =>
                    if seen[place] {
                        return false
                    } else {
                        seen[place] = true
                    },
                _ => ()
            }
        }
    }

    return true;
}

fn search_games_dfs(pieces: usize, visitor: &mut ModalVisitor, p1state: &mut Vec<usize>, p2state: &mut Vec<usize>) {
    if p1state.len() == pieces && p2state.len() == pieces {
        visitor.found(p1state, p2state)
    } else {
        // states are ordered monotonically; if the last move is the starting or ending pool,
        // we can create a new state in the same pool. Otherwise, place in the next potentially
        // valid spot.
        let min_i = match (*p1state).last() {
            Some(0) => 0,
            Some(15) => 15,
            Some(min) => min + 1,
            None => 0
        };
        let min_j = match (*p2state).last() {
            Some(0) => 0,
            Some(15) => 15,
            Some(min) => min + 1,
            None => 0
        };

        for i in min_i..16 {
            p1state.push(i);
            for j in min_j..16 {
                p2state.push(j);
                if is_valid(p1state, p2state) {
                    search_games_dfs(pieces, visitor, p1state, p2state);
                }
                p2state.pop();
            }
            p1state.pop();
        }
    }
}

fn search_games(pieces: u8, visitor: &mut ModalVisitor) {
    search_games_dfs(usize::from(pieces), visitor, Vec::with_capacity(usize::from(pieces)).as_mut(), Vec::with_capacity(usize::from(pieces)).as_mut());
}

#[cfg(test)]
mod tests {

    mod is_valid {
        use super::super::*;

        #[test]
        fn test_starting_pool() {
            assert!(is_valid(&vec![0], &Vec::new()));
            assert!(is_valid(&vec![0, 0], &Vec::new()));
            assert!(is_valid(&vec![0, 0, 0], &Vec::new()));
            assert!(is_valid(&Vec::new(), &vec![0]));
            assert!(is_valid(&vec![0], &vec![0]));
        }

        #[test]
        fn test_ending_pool() {
            assert!(is_valid(&vec![15], &Vec::new()));
            assert!(is_valid(&vec![15, 15], &Vec::new()));
            assert!(is_valid(&vec![15, 15, 15], &Vec::new()));
            assert!(is_valid(&Vec::new(), &vec![15]));
            assert!(is_valid(&vec![15], &vec![15]));
        }

        #[test]
        fn test_single_player_duplicate() {
            assert!(!is_valid(&vec![1, 1], &Vec::new()));
            assert!(is_valid(&vec![1, 2], &Vec::new()));
            assert!(!is_valid(&Vec::new(), &vec![1, 1]));
            assert!(is_valid(&Vec::new(), &vec![1, 2]));
        }

        #[test]
        fn test_multi_player_duplicate() {
            assert!(is_valid(&vec![5, 6], &vec![11, 12]));
            assert!(!is_valid(&vec![5, 11], &vec![11, 12]));
            assert!(!is_valid(&vec![5, 6], &vec![11, 6]));
        }
    }

    mod count_branch {
        use super::super::count_branch;

        #[test]
        fn test_empty_places_has_zero_branches() {
            assert_eq!(count_branch(&vec![]), 0);
        }

        #[test]
        fn test_single_zero_has_one_branch() {
            assert_eq!(count_branch(&vec![0usize]), 1);
        }

        #[test]
        fn test_single_15_has_zero_branches() {
            assert_eq!(count_branch(&vec![15usize]), 0);
        }

        #[test]
        fn test_single_5_has_one_branch() {
            assert_eq!(count_branch(&vec![5usize]), 1);
        }

        #[test]
        fn test_two_zeroes_have_one_branch() {
            assert_eq!(count_branch(&vec![0usize, 0usize]), 1);
        }

        #[test]
        fn test_sequence_has_expected_three_branches() {
            assert_eq!(count_branch(&vec![0usize, 0usize, 0usize, 2usize, 7usize, 15usize, 15usize]), 3);
        }
    }
}

// count the number of branches possible from this placement. The branch count
// is equal to the sum of 1 for the first zero placement, 0 for any 15 placement,
// and 1 otherwise.
fn count_branch(places: &[usize]) -> u8 {
    let mut seen_zero = false;
    let mut branches = 0u8;

    for p in places.iter() {
        match *p {
            0 if !seen_zero => {
                branches += 1;
                seen_zero = true;
            },
            0 => (),
            15 => (),
            _ => {
                branches += 1;
            }
        }
    }

    return branches;
}

pub struct ModalVisitor {
    mode: char,
    count: u64,
    count_branch_p1: u64,
    count_branch_p2: u64
}

trait Visitor {
    fn found(&mut self, p1: &[usize], p2: &[usize]) -> ();

    fn end(&mut self) -> ();
}

impl Visitor for ModalVisitor {
    fn found(&mut self, p1: &[usize], p2: &[usize]) -> () {
        self.count += 1;
        self.count_branch_p1 += u64::from(count_branch(p1));
        self.count_branch_p2 += u64::from(count_branch(p2));
        if self.mode == 'p' {
            println!("{:?}; {:?}", p1, p2);
        }
    }

    fn end(&mut self) -> () {
        if self.mode == 'c' {
            println!("states: {} p1_branches: {} p2_branches: {}", self.count, self.count_branch_p1, self.count_branch_p2);
        }
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();

    let pieces_arg = args.get(1);
    let mode_arg = args.get(2);

    if pieces_arg.is_none() || mode_arg.is_none() {
        eprintln!("syntax: <pieces> <mode>, where 1 <= pieces <= 7 and mode in print | count");
        process::exit(1);
    } else {
        let pieces: u8 = pieces_arg.unwrap().parse().unwrap();
        let mode: char = match mode_arg.unwrap().as_str() {
            "print" => 'p',
            "count" => 'c',
            _ => panic!("invalid mode")
        };
        let mut visitor = ModalVisitor { mode, count: 0, count_branch_p1: 0, count_branch_p2: 0 };

        search_games(pieces, visitor.borrow_mut());
        visitor.end();
    }
}
